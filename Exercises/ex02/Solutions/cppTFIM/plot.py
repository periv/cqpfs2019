import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("data.dat")

# plot magnetization
plt.pcolor(data,
           vmin=-1.0, vmax=1.0)
plt.xlabel('site')
plt.ylabel('time')
#plt.axis([0,l,tstart,tend])
plt.title("Magnetisation")
plt.colorbar()
plt.show()
